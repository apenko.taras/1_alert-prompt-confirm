let name = prompt("What's your name?")
let age = prompt("How old are you?")

if (age < 18) {
    alert("You are not allowed to visit this website")
} else if (age > 22) {
    alert("Welcome, " + name)
} else {
    let isContinue = confirm("Are you sure you want to continue?");
    if (isContinue === true) {
        alert("Welcome, " + name)
    } else {
        alert("You are not allowed to visit this website")
    }
}
